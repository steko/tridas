************************************
Setting up development environment for 
DendroLibrary on Kubuntu.

31st May 2010
************************************


1)  Install Eclipse (I'm using 3.5.2), maven2, java6 and subversion from standard repositories (e.g. sudo apt-get install eclipse maven2 subversion sun-java6-jdk).  You may need to turn on the canonical partner repositories to get sun java6.

2)  Open Eclipse and install the subversive plugin by going to Help > Install new software.  You'll need to add the update site http://download.eclipse.org/releases/galileo.  The subversive plugin can be found under Collaboration > Subversive SVN Team provider.

3)  Install m2eclipse plugin from within Eclipse in the same way.  The update site is http://m2eclipse.sonatype.org/sites/m2e

4)  Checkout DendroLibrary project from SF repository.  In eclipse go to File > New > Other, then select SVN > Project from SVN.  Use the repository URL https://tridas.svn.sourceforge.net/svnroot/tridas/DendroLibrary.  Eclipse should prompt you to install SVN connectors the first time you try to use subversive unless you are using an older version of Eclipse, in which case you'll need to install these manually.

5)  Right click on the DendroLibrary project and select properties.  Select Java Build Path on the left and highlight the JRE System Library on the right.  Click edit and make sure it is set to the working Java 6 JRE that is installed on your system.  

6)  Now checkout the following projects in the same way:
    - https://tridas.svn.sourceforge.net/svnroot/tridas/DendroLibrary/TridasJLib 
    - https://tridas.svn.sourceforge.net/svnroot/tridas/DendroLibrary/DendroFileIO
    - https://tridas.svn.sourceforge.net/svnroot/tridas/DendroLibrary/DendroFileIOGui

7)  As before, make sure each project is set to use a suitable Java6 JRE installed on your system.

8)  From your command line navigate to the Libraries folder of the TridasJLib project, then run the MavenInstallCommands.sh script.

9)  Within Eclipse right click on the DendroLibrary/pom.xml file and choose Run as > Maven install.  You should only ever have to do this once on a system as this master project should never change.  It may take some time as Maven downloads all the dependencies.

10) Once Maven has finished, then you can right click the TridasJLib/pom.xml file and do Run as > Maven package.

11) Now do Run as > Maven package on the DendroFileIO/pom.xml file.

12) To test that all is working fine try running the DendroFileIOGui application by doing right click Run as > Java Application on DendroFileIOGui/org.tridas.io.gui/App.java.

